package com.nikosstathis.countryweather.rest;

import com.nikosstathis.countryweather.dto.ControllerExceptionError;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.sql.Timestamp;

/**
 * GlobalRestExceptionHandler is used so as to handle all rest exceptions in a uniform  way, responding with a
 *
 * @{@link ControllerExceptionError}
 */
@ControllerAdvice
@Slf4j
public class GlobalRestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({IllegalArgumentException.class})
    public ResponseEntity<Object> handleIllegalArgumentException(Exception ex, WebRequest request) {
        log.error("IllegalArgumentException:"+ ex.getMessage());
        return createResponseEntity(HttpStatus.BAD_REQUEST, ex, request);
    }

    private ResponseEntity<Object> createResponseEntity(HttpStatus httpStatus, Exception ex, WebRequest request) {
        return new ResponseEntity<>(
                new ControllerExceptionError(
                        new Timestamp(System.currentTimeMillis()) + "",
                        httpStatus.value(),
                        httpStatus.name(),
                        ex.getMessage(),
                        ((ServletWebRequest) request).getRequest().getServletPath()
                ), new HttpHeaders(), httpStatus
        );
    }

}

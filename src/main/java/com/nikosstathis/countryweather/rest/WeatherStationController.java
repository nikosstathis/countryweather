package com.nikosstathis.countryweather.rest;

import com.nikosstathis.countryweather.dto.WeatherStationCurrentInfo;
import com.nikosstathis.countryweather.service.WeatherStationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/*
 * This is the entry point of our RESTful service
 */
@RestController
public class WeatherStationController {

    @Autowired
    WeatherStationService weatherStationService;

    @GetMapping("/weatherstationcurrentinfo")
    List<WeatherStationCurrentInfo> getWeatherObservations(@RequestParam String country){
        return weatherStationService.getWeatherStationCurrentInfos(country);
    }

}

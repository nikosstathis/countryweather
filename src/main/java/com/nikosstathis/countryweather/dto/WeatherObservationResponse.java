package com.nikosstathis.countryweather.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WeatherObservationResponse {
    List<WeatherObservation> weatherObservations;
}

package com.nikosstathis.countryweather.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WeatherStationCurrentInfo {
    String name;
    BigDecimal temperature;
}

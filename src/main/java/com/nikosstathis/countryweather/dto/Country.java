package com.nikosstathis.countryweather.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Country {
    @XmlElement
    String countryCode;
    @XmlElement
    String countryName;
    @XmlElement
    String isoNumeric;
    @XmlElement
    String isoAlpha3;
    @XmlElement
    String fipsCode;
    @XmlElement
    String continent;
    @XmlElement
    String continentName;
    @XmlElement
    String capital;
    @XmlElement
    BigDecimal areaInSqKm;
    @XmlElement
    Long population;
    @XmlElement
    String currencyCode;
    @XmlElement
    String languages;
    @XmlElement
    Long geonameId;
    @XmlElement
    BigDecimal west;
    @XmlElement
    BigDecimal north;
    @XmlElement
    BigDecimal east;
    @XmlElement
    BigDecimal south;
    @XmlElement
    String postalCodeFormat;
}

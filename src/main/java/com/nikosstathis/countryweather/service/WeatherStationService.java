package com.nikosstathis.countryweather.service;

import com.nikosstathis.countryweather.dto.WeatherStationCurrentInfo;
import org.springframework.stereotype.Service;

import java.util.List;

/*
 * The orchestrator service that will use the other services to deliver the results
 */
@Service
public interface WeatherStationService {

    /**
     * Takes as argument a country code and returs all the weather observations for this country
     */
    List<WeatherStationCurrentInfo> getWeatherStationCurrentInfos(String iso2CountryCode);

}

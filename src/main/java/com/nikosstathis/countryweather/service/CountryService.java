package com.nikosstathis.countryweather.service;

import com.nikosstathis.countryweather.dto.Country;
import com.nikosstathis.countryweather.dto.Geonames;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * This service is used to retrieve data for the countries
 */
@Service
public interface CountryService {

    /**
     * We use this method so as to get the coordinates for a country
     *
     */
    Country getCountryInfo(String countryCode);

    /**
     * This is a reverse lookup method, we provide the lat/lng and see which country these correspond to
     */
    String getCountryCode(BigDecimal lat, BigDecimal lng);

    /**
     * This method is used to retrieve info for all the countries
     */
    Geonames getAllCountryInfos();

}

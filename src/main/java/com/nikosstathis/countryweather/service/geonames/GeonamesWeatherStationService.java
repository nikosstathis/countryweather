package com.nikosstathis.countryweather.service.geonames;

import com.nikosstathis.countryweather.dto.Country;
import com.nikosstathis.countryweather.dto.WeatherStationCurrentInfo;
import com.nikosstathis.countryweather.service.CountryService;
import com.nikosstathis.countryweather.service.WeatherObservationService;
import com.nikosstathis.countryweather.service.WeatherStationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/*
 * The implementation of the WeatherStationService using the Geonames services.
 * This is the orchestrator service, calling the other services
 */
@Service
public class GeonamesWeatherStationService implements WeatherStationService {

    @Autowired
    CountryService countryService;

    @Autowired
    WeatherObservationService weatherObservationService;

    @Override
    public List<WeatherStationCurrentInfo> getWeatherStationCurrentInfos(String iso2CountryCode) {
        Country countryInfo = countryService.getCountryInfo(iso2CountryCode);

        return weatherObservationService.getWeatherObservations(
                countryInfo.getWest(),
                countryInfo.getNorth(),
                countryInfo.getEast(),
                countryInfo.getSouth()
        ).getWeatherObservations().stream()
                .filter(info -> countryService.getCountryCode(info.getLat(), info.getLng()).trim().equals(iso2CountryCode))
                .map(info -> new WeatherStationCurrentInfo(info.getStationName().trim(), info.getTemperature()))
                .collect(Collectors.toList());
    }
}

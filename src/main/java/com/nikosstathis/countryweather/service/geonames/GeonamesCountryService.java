package com.nikosstathis.countryweather.service.geonames;

import com.nikosstathis.countryweather.dto.Country;
import com.nikosstathis.countryweather.dto.Geonames;
import com.nikosstathis.countryweather.service.CountryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Locale;

/*
 * The implementation of the CountryService using the Geonames services.
 * We use the Cachable annotation on the methods so that each method is
 * executed only once. All succeeding calls will just get the cached result
 */
@Service
@Slf4j
public class GeonamesCountryService implements CountryService {

    private static final String COUNTRY_CODE_URL_PARAMS = "?lat={lat}&lng={lng}&username={username}";
    private static final String COUNTRY_INFO_URL_PARAMS = "?username={username}";

    @Value("${geonames.username}")
    String geonamesUsername;

    @Value("${geonames.countrycodeurl}")
    String geonamesCountryCodeUrl;

    @Value("${geonames.countryinfourl}")
    String geonamesCountryInfoUrl;

    @Autowired
    RestTemplate restTemplate;

    @Override
    @Cacheable("countries")
    public Country getCountryInfo(String countryCode) {
        validateCountryCode(countryCode);
        log.info("Sending request for country:" + countryCode);
        return getAllCountryInfos().getCountries()
                .stream()
                .filter(countryInfo -> countryInfo.getCountryCode().equals(countryCode))
                .findFirst().orElseThrow(() -> new RuntimeException("invalid country code"));
    }

    @Override
    @Cacheable("reverseCountryLookup")
    public String getCountryCode(BigDecimal lat, BigDecimal lng) {
        log.info("Sending request for countryCode for lat:" + lat+", lng:"+lng);
        return restTemplate.getForObject(geonamesCountryCodeUrl + COUNTRY_CODE_URL_PARAMS,
                String.class, lat, lng, geonamesUsername);
    }

    @Override
    @Cacheable("allCountries")
    public Geonames getAllCountryInfos() {
        log.info("Sending request for all Countries");
        return restTemplate.getForObject(geonamesCountryInfoUrl + COUNTRY_INFO_URL_PARAMS,
                Geonames.class, geonamesUsername);
    }

    void validateCountryCode(String country) {
        if(country == null || !Arrays.asList(Locale.getISOCountries()).contains(country)){
            throw new IllegalArgumentException("invalid country code:"+country);
        }
    }

}

package com.nikosstathis.countryweather.service.geonames;

import com.nikosstathis.countryweather.dto.WeatherObservationResponse;
import com.nikosstathis.countryweather.service.WeatherObservationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;

/*
 * The implementation of the WeatherObservationService using the Geonames services.
 * We cannot use caching mechanisms here because the retrieved temperatures may
 * differ on succeeding method calls
 */
@Service
@Slf4j
public class GeonamesWeatherObservationService implements WeatherObservationService {

    private static final String WEATHER_JSON_URL_PARAMS = "?west={west}&north={north}&east={east}&south={south}&username={username}&maxRows={maxRows}";

    @Value("${geonames.username}")
    String geonamesUsername;

    @Value("${geonames.weatherjsonurl}")
    String geonamesWeatherJsonUrl;

    @Value("${geonames.maxrows}")
    long maxRows;

    @Autowired
    RestTemplate restTemplate;

    @Override
    public WeatherObservationResponse getWeatherObservations(BigDecimal west, BigDecimal north, BigDecimal east, BigDecimal south) {
        log.info("Sending request for weatherJson for west:"+west+", north:"+north+", east:"+east+", south:"+south);
        return restTemplate.getForObject(geonamesWeatherJsonUrl + WEATHER_JSON_URL_PARAMS,
                WeatherObservationResponse.class, west, north, east, south, geonamesUsername, maxRows);
    }

}

package com.nikosstathis.countryweather.service;

import com.nikosstathis.countryweather.dto.WeatherObservationResponse;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/*
 * This service is used to return the weather observations inside specific coordinates
 */
@Service
public interface WeatherObservationService {

    /*
     * Returns all the weather station info inside these coordinates
     */
    WeatherObservationResponse getWeatherObservations(BigDecimal west, BigDecimal north, BigDecimal east, BigDecimal south);

}

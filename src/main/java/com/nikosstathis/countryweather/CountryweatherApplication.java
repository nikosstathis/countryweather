package com.nikosstathis.countryweather;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

/*
 * This is the starting point of our application, the main method that loads the SpringBoot app
 */
@SpringBootApplication
@EnableCaching
@EnableWebSecurity
public class CountryweatherApplication {

	public static void main(String[] args) {
		SpringApplication.run(CountryweatherApplication.class, args);
	}

}


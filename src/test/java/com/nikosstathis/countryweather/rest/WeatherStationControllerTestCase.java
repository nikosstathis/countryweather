package com.nikosstathis.countryweather.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nikosstathis.countryweather.dto.WeatherStationCurrentInfo;
import com.nikosstathis.countryweather.service.WeatherStationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;


import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Tests for the WeatherStationController using Mockito and  MockMvc
 */
@RunWith(MockitoJUnitRunner.class)
public class WeatherStationControllerTestCase {

    private MockMvc mvc;

    @InjectMocks
    private WeatherStationController weatherStationController;

    @Mock
    private WeatherStationService weatherStationService;

    @Before
    public void setup() {
        mvc = MockMvcBuilders.standaloneSetup(weatherStationController)
                .setControllerAdvice(new GlobalRestExceptionHandler())
                .build();
    }

    /**
     * tests weather observations restful service
     */
    @Test
    public void getWeatherObservationsForGreece() throws Exception {
        List<WeatherStationCurrentInfo> weatherObservationsForGreece = Arrays.asList(
                new WeatherStationCurrentInfo("SOUDA BAY NAS", new BigDecimal(9.4)),
                new WeatherStationCurrentInfo("Kerkyra Airport", new BigDecimal(14)),
                new WeatherStationCurrentInfo("Tanagra Airport", new BigDecimal(4))
        );
        when(weatherStationService.getWeatherStationCurrentInfos("GR"))
                .thenReturn(weatherObservationsForGreece);

        final String requestBody = new ObjectMapper().writeValueAsString(weatherObservationsForGreece);

        this.mvc.perform(get("/weatherstationcurrentinfo/?country=GR")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
                .andExpect(status().isOk())
                .andExpect(content().string(requestBody));
    }

    /**
     * tests weather observations restful service for an invalid input
     */
    @Test
    public void getWeatherObservationsForInvalidCountryCode() throws Exception {
        String invalidCountryCode = "GR123";
        when(weatherStationService.getWeatherStationCurrentInfos(invalidCountryCode))
                .thenThrow(new IllegalArgumentException("invalid country code:"+invalidCountryCode));


        this.mvc.perform(get("/weatherstationcurrentinfo/?country=" + invalidCountryCode))
                .andExpect(status().isBadRequest());
    }
}

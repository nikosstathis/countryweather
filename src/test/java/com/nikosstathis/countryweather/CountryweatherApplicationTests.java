package com.nikosstathis.countryweather;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CountryweatherApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Test
	public void applicationContextTest() {
		CountryweatherApplication.main(new String[] {});
	}
}


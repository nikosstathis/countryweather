package com.nikosstathis.countryweather.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nikosstathis.countryweather.dto.WeatherObservation;
import com.nikosstathis.countryweather.dto.WeatherObservationResponse;
import com.nikosstathis.countryweather.service.geonames.RestTemplateConfiguration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.match.MockRestRequestMatchers;
import org.springframework.test.web.client.response.MockRestResponseCreators;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.Arrays;

/**
 * Tests for the WeatherObservationService
 */
@Import({
        RestTemplateConfiguration.class
})
@RunWith(SpringRunner.class)
@RestClientTest(WeatherObservationService.class)
public class WeatherObservationServiceTestCase {

    @Autowired
    WeatherObservationService weatherObservationService;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    private MockRestServiceServer server;

    @Value("${geonames.username}")
    String geonamesUsername;

    @Value("${geonames.weatherjsonurl}")
    String geonamesWeatherJsonUrl;

    @Value("${geonames.maxrows}")
    long maxRows;

    @Before
    public void setUp() {
        server = MockRestServiceServer.createServer(restTemplate);
    }

    @Test
    public void getWeatherObservations() throws Exception {
        BigDecimal west = new BigDecimal("5.735698938390786");
        BigDecimal north = new BigDecimal("50.182772453796446");
        BigDecimal south = new BigDecimal("49.447858677765716");
        BigDecimal east = new BigDecimal("6.5308980672559525");

        WeatherObservation luxemburgObservation = new WeatherObservation(
                "Luxembourg / Luxembourg",
                new BigDecimal(1.2),
                new BigDecimal(49.61666666666667),
                new BigDecimal(6.216666666666667)
        );

        String responseBody = new ObjectMapper().writeValueAsString(
                new WeatherObservationResponse(Arrays.asList(luxemburgObservation))
        );

        this.server.expect(MockRestRequestMatchers.requestTo(geonamesWeatherJsonUrl + "?west=" + west + "&north="
                + north + "&east=" + east + "&south=" + south + "&username=" + geonamesUsername+ "&maxRows=" + maxRows))
                .andRespond(MockRestResponseCreators.withSuccess()
                        .body(responseBody)
                        .contentType(MediaType.APPLICATION_JSON)
                );
        WeatherObservationResponse response = weatherObservationService.getWeatherObservations(west, north, east, south);

        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getWeatherObservations());
        Assert.assertEquals(1, response.getWeatherObservations().size());

        WeatherObservation observationInResponse = response.getWeatherObservations().iterator().next();
        Assert.assertEquals(observationInResponse.getLat(), luxemburgObservation.getLat());
        Assert.assertEquals(observationInResponse.getLng(), luxemburgObservation.getLng());
        Assert.assertEquals(observationInResponse.getStationName(), luxemburgObservation.getStationName());
        Assert.assertEquals(observationInResponse.getTemperature(), luxemburgObservation.getTemperature());
    }

}

package com.nikosstathis.countryweather.service;

import com.nikosstathis.countryweather.dto.Country;
import com.nikosstathis.countryweather.dto.Geonames;
import com.nikosstathis.countryweather.service.geonames.RestTemplateConfiguration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.match.MockRestRequestMatchers;
import org.springframework.test.web.client.response.MockRestResponseCreators;
import org.springframework.web.client.RestTemplate;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * Tests for the CountryService
 */
@Import({
        RestTemplateConfiguration.class
})
@RunWith(SpringRunner.class)
@RestClientTest(CountryService.class)
public class CountryServiceTestCase {

    @Autowired
    CountryService countryService;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    private MockRestServiceServer server;

    @Value("${geonames.username}")
    String geonamesUsername;

    @Value("${geonames.countrycodeurl}")
    String geonamesCountryCodeUrl;

    @Value("${geonames.countryinfourl}")
    String geonamesCountryInfoUrl;

    @Before
    public void setUp() {
        server = MockRestServiceServer.createServer(restTemplate);
    }

    @Test
    public void getCountryInfoForGreece() throws JAXBException {
        final Country greece = CountryUtils.createCountryGreece();
        final List<Country> countryList = Arrays.asList(greece, CountryUtils.createCountryAfghanistan());
        JAXBContext jaxbContext = JAXBContext.newInstance(Geonames.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        StringWriter sw = new StringWriter();
        jaxbMarshaller.marshal(new Geonames(countryList), sw);
        String xmlString = sw.toString();

        this.server.expect(MockRestRequestMatchers.requestTo(geonamesCountryInfoUrl + "?username=" + geonamesUsername))
                .andRespond(
                        MockRestResponseCreators.withSuccess()
                                .body(xmlString)
                                .contentType(MediaType.TEXT_XML)
                )
        ;

        Country response = countryService.getCountryInfo("GR");

        Assert.assertNotNull(response);
        Assert.assertEquals(greece.getCountryCode(), response.getCountryCode());
        Assert.assertEquals(greece.getNorth(), response.getNorth());
        Assert.assertEquals(greece.getSouth(), response.getSouth());
        Assert.assertEquals(greece.getEast(), response.getEast());
        Assert.assertEquals(greece.getWest(), response.getWest());
    }

    @Test
    public void reverseLookupForGreekWeatherStation() {
        BigDecimal lat = new BigDecimal(35.53333333333333);
        BigDecimal lng = new BigDecimal(24.15);
        String greeceCountryCode = "GR";

        this.server.expect(MockRestRequestMatchers.requestTo(geonamesCountryCodeUrl + "?lat=" + lat + "&lng=" + lng + "&username=" + geonamesUsername))
                .andRespond(MockRestResponseCreators.withSuccess()
                        .body(greeceCountryCode)
                );

        String response = countryService.getCountryCode(lat, lng);
        Assert.assertEquals(greeceCountryCode, response);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getCountryInfoForNull(){ //test that an illegal argument exception is thrown when no arg is passed
        countryService.getCountryInfo(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getCountryInfoForInvalidCountry(){
        countryService.getCountryInfo("thisInNotACountryCode");
    }
}

package com.nikosstathis.countryweather.service;

import com.nikosstathis.countryweather.dto.Country;
import com.nikosstathis.countryweather.dto.WeatherObservation;
import com.nikosstathis.countryweather.dto.WeatherObservationResponse;
import com.nikosstathis.countryweather.dto.WeatherStationCurrentInfo;
import com.nikosstathis.countryweather.service.geonames.GeonamesWeatherStationService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * Tests for the WeatherStationService
 */
@RunWith(MockitoJUnitRunner.class)
public class WeatherStationServiceTestCase {

    @Mock
    CountryService countryService;

    @Mock
    WeatherObservationService weatherObservationService;

    @InjectMocks
    WeatherStationService weatherStationService = new GeonamesWeatherStationService();

    @Test
    public void getWeatherStationInfo() {
        Country greece = CountryUtils.createCountryGreece();

        //this station is in Greece
        WeatherObservation skiros = new WeatherObservation(
                "SKIROS",
                new BigDecimal(1.3),
                new BigDecimal(38.96666666666667),
                new BigDecimal(24.483333333333334)
        );
        //this station is inside the sourounding box of Greece but not in Greece
        WeatherObservation balikeshir = new WeatherObservation(
                "Balikesir",
                new BigDecimal(2.5),
                new BigDecimal(39.61666666666667),
                new BigDecimal(27.916666666666668)
        );

        Mockito.when(countryService.getCountryInfo("GR")).thenReturn(greece);
        Mockito.when(weatherObservationService.getWeatherObservations(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(new WeatherObservationResponse(Arrays.asList(skiros, balikeshir)));
        Mockito.when(countryService.getCountryCode(skiros.getLat(), skiros.getLng())).thenReturn(greece.getCountryCode());
        Mockito.when(countryService.getCountryCode(balikeshir.getLat(), balikeshir.getLng())).thenReturn("TR");
        List<WeatherStationCurrentInfo> ret = weatherStationService.getWeatherStationCurrentInfos("GR");

        Assert.assertNotNull(ret);
        Assert.assertEquals(1, ret.size());
        Assert.assertEquals(Arrays.asList(new WeatherStationCurrentInfo(skiros.getStationName(), skiros.getTemperature())), ret);
    }

}

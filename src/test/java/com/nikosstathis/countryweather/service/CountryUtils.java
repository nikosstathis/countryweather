package com.nikosstathis.countryweather.service;

import com.nikosstathis.countryweather.dto.Country;

import java.math.BigDecimal;

public class CountryUtils {
    public static Country createCountryAfghanistan() {
        return new Country(
                "AF",
                "Afghanistan",
                "004",
                "AFG",
                "AF",
                "AS",
                "Asia",
                "Kabul",
                new BigDecimal(647500.0),
                29121286l,
                "AFN",
                "fa-AF,ps,uz-AF,tk",
                1149361l,
                new BigDecimal(60.4720833972263),
                new BigDecimal(38.4907920755748),
                new BigDecimal(74.8894511481168),
                new BigDecimal("29.3770645357176"),
                null
        );
    }

    public static Country createCountryGreece() {
        return new Country(
                "GR",
                "Greece",
                "300",
                "GRC",
                "GR",
                "EU",
                "Europe",
                "Athens",
                new BigDecimal(131940.0),
                11000000l,
                "EUR",
                "el-GR,en,fr",
                390903l,
                new BigDecimal(19.3736035624134),
                new BigDecimal(41.7484999849641),
                new BigDecimal(28.2470831714347),
                new BigDecimal(34.8020663391466),
                "### ##"
        );
    }
}
